/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/


#pragma once

#include "OVMPropertyVisualizer.hh"

#include <ACG/Scenegraph/LineNode.hh>
#include <ACG/Scenegraph/MeshNode2T.hh>
#include "Nodes/BoxesNode.hh"

#include <OpenFlipper/BasePlugin/PluginFunctions.hh>

#include <OpenVolumeMesh/Core/BaseEntities.hh>

#include "Widgets/Matrix3x3Widget.hh"

#include <iostream>

template <typename MeshT>
class OVMPropertyVisualizerMatrix3x3: public OVMPropertyVisualizer<MeshT>{

public:
    OVMPropertyVisualizerMatrix3x3(MeshT* _mesh, int objectID, PropertyInfo _propertyInfo);
    virtual ~OVMPropertyVisualizerMatrix3x3(){ clear(); }

    void clear() override;

protected:
    void duplicateProperty() override;


    void visualizeFaceProp(bool _setDrawMode = true) override;
    void visualizeEdgeProp(bool _setDrawMode = true) override;
    void visualizeHalfedgeProp(bool _setDrawMode = true) override;
    void visualizeVertexProp(bool _setDrawMode = true) override;
    void visualizeCellProp(bool _setDrawMode = true) override;
    void visualizeHalffaceProp(bool _setDrawMode = true) override;

    QString getPropertyText(unsigned int index) override;

    ACG::SceneGraph::LineNode*  lineNode;
    ACG::SceneGraph::BoxesNode* boxesNode;

private:
    template<typename EntityTag, typename Property, typename EntityIterator>
    void visualizeAsCrossesForEntity(Property prop, EntityIterator e_begin, EntityIterator e_end);
    template<typename EntityTag, typename Property, typename EntityIterator>
    void visualizeAsBoxesForEntity(Property prop, EntityIterator e_begin, EntityIterator e_end);
    template<typename EntityTag, typename EntityIterator>
    void visualizeForEntity(EntityIterator e_begin, EntityIterator e_end);

    Matrix3x3Widget* getMatWidget() {return static_cast<Matrix3x3Widget*>(PropertyVisualizer::widget);}

};

#if defined(INCLUDE_TEMPLATES) && !defined(OVM_PROPERTY_VISUALIZER_MAT3X3_CC)
#include "OVMPropertyVisualizerMatrix3x3_impl.hh"
#endif

