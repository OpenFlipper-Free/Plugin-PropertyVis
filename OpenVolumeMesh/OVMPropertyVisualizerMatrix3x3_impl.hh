/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/




#define OVM_PROPERTY_VISUALIZER_MAT3X3_CC

#include <ACG/Utils/ColorConversion.hh>
#include "OVMPropertyVisualizerMatrix3x3.hh"
#include "ACG/Scenegraph/DrawModes.hh"
#include "EntityPosition.hh"

using Color4f = ACG::SceneGraph::LineNode::Color4f;
static std::array<Color4f, 3> dim_color {
    Color4f{1.f,0.f,0.f,1.f},
    Color4f{0.f,1.f,0.f,1.f},
    Color4f{0.f,0.f,1.f,1.f}
};

template <typename MeshT>
OVMPropertyVisualizerMatrix3x3<MeshT>::OVMPropertyVisualizerMatrix3x3(MeshT* _mesh, int objectID, PropertyInfo _propertyInfo)
    : OVMPropertyVisualizer<MeshT>(_mesh, objectID, _propertyInfo)
{
    if (PropertyVisualizer::widget) delete PropertyVisualizer::widget;
    Matrix3x3Widget* w = new Matrix3x3Widget();
    PropertyVisualizer::widget = w;

    BaseObjectData *bod;
    PluginFunctions::getObject(objectID, bod);

    boxesNode = new ACG::SceneGraph::BoxesNode(bod->manipulatorNode());

    lineNode = new ACG::SceneGraph::LineNode(ACG::SceneGraph::LineNode::LineSegmentsMode, bod->manipulatorNode());
    this->connect(w->lineWidth, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
                  [this](double value) {lineNode->set_line_width(value);});
}

template <typename MeshT>
void OVMPropertyVisualizerMatrix3x3<MeshT>::clear()
{
    lineNode->clear();
    boxesNode->clear();
    OVMPropertyVisualizer<MeshT>::clear();
}

template <typename MeshT>
void OVMPropertyVisualizerMatrix3x3<MeshT>::duplicateProperty()
{
    OVMPropertyVisualizer<MeshT>::template duplicateProperty_stage1<ACG::Matrix3x3d>();
}

template<typename MeshT>
template<typename EntityTag, typename Property, typename EntityIterator>
void OVMPropertyVisualizerMatrix3x3<MeshT>::
visualizeAsCrossesForEntity(Property prop, EntityIterator e_begin, EntityIterator e_end)
{
    using ACG::Vec3d;
    using ACG::Matrix3x3d;

    MeshT &m = *OVMPropertyVisualizer<MeshT>::mesh;
    EntityPosition<MeshT> ep{m};


    double scaleFactor = getMatWidget()->getScaleFactor();

    for (EntityIterator e_it = e_begin; e_it != e_end; ++e_it) {
        Matrix3x3d mat = prop[*e_it];
        Vec3d center_pos = ep(*e_it);
        for (unsigned char dim = 0; dim < 3; ++dim) {
            ACG::Vec3d v = mat.getCol(dim) * scaleFactor;
            lineNode->add_line(center_pos - v, center_pos + v);
            lineNode->add_color(dim_color[dim]);
        }
    }
}

template<typename MeshT>
template<typename EntityTag, typename Property, typename EntityIterator>
void OVMPropertyVisualizerMatrix3x3<MeshT>::
visualizeAsBoxesForEntity(Property prop, EntityIterator e_begin, EntityIterator e_end)
{
    using ACG::Vec3d;
    using ACG::Matrix3x3d;
    MeshT &m = *OVMPropertyVisualizer<MeshT>::mesh;
    EntityPosition<MeshT> ep{m};


    for (EntityIterator e_it = e_begin; e_it != e_end; ++e_it) {
        const Matrix3x3d &mat = prop[*e_it];
        Vec3d center_pos = ep(*e_it);
        boxesNode->push_back({mat, center_pos});
    }
    boxesNode->setScaleFactor(getMatWidget()->getScaleFactor());
}

template<typename MeshT>
template<typename EntityTag, typename EntityIterator>
void OVMPropertyVisualizerMatrix3x3<MeshT>::
visualizeForEntity(EntityIterator e_begin, EntityIterator e_end)
{
    clear();
    MeshT &m = * OVMPropertyVisualizer<MeshT>::mesh;
    auto prop = m.template request_property<ACG::Matrix3x3d, EntityTag>(OVMPropertyVisualizer<MeshT>::propertyInfo.propName());
    if (!prop)
        throw VizException("Getting PropHandle from mesh for selected property failed.");

    if (getMatWidget()->as_crosses->isChecked()) {
        visualizeAsCrossesForEntity<EntityTag>(prop, e_begin, e_end);
    } else if (getMatWidget()->as_boxes->isChecked()) {
        visualizeAsBoxesForEntity<EntityTag>(prop, e_begin, e_end);
    }
}

template <typename MeshT>
void OVMPropertyVisualizerMatrix3x3<MeshT>::visualizeCellProp(bool /*_setDrawMode*/)
{
    MeshT &m = * OVMPropertyVisualizer<MeshT>::mesh;
    visualizeForEntity<OpenVolumeMesh::Entity::Cell>(m.cells_begin(), m.cells_end());
}

template <typename MeshT>
void OVMPropertyVisualizerMatrix3x3<MeshT>::visualizeFaceProp(bool /*_setDrawMode*/)
{
    MeshT &m = * OVMPropertyVisualizer<MeshT>::mesh;
    visualizeForEntity<OpenVolumeMesh::Entity::Face>(m.faces_begin(), m.faces_end());
}


template <typename MeshT>
void OVMPropertyVisualizerMatrix3x3<MeshT>::visualizeHalffaceProp(bool /*_setDrawMode*/)
{
    MeshT &m = * OVMPropertyVisualizer<MeshT>::mesh;
    visualizeForEntity<OpenVolumeMesh::Entity::HalfFace>(m.halffaces_begin(), m.halffaces_end());
}

template <typename MeshT>
void OVMPropertyVisualizerMatrix3x3<MeshT>::visualizeEdgeProp(bool /*_setDrawMode*/)
{
    MeshT &m = * OVMPropertyVisualizer<MeshT>::mesh;
    visualizeForEntity<OpenVolumeMesh::Entity::Edge>(m.edges_begin(), m.edges_end());
}

template <typename MeshT>
void OVMPropertyVisualizerMatrix3x3<MeshT>::visualizeHalfedgeProp(bool /*_setDrawMode*/)
{
    MeshT &m = * OVMPropertyVisualizer<MeshT>::mesh;
    visualizeForEntity<OpenVolumeMesh::Entity::HalfEdge>(m.halfedges_begin(), m.halfedges_end());
}

template <typename MeshT>
void OVMPropertyVisualizerMatrix3x3<MeshT>::visualizeVertexProp(bool /*_setDrawMode*/)
{
    MeshT &m = * OVMPropertyVisualizer<MeshT>::mesh;
    visualizeForEntity<OpenVolumeMesh::Entity::Vertex>(m.vertices_begin(), m.vertices_end());
}

template <typename MeshT>
QString OVMPropertyVisualizerMatrix3x3<MeshT>::getPropertyText(unsigned int index)
{
    return OVMPropertyVisualizer<MeshT>::template getPropertyText_<ACG::Matrix3x3d>(index);
}

