#pragma once

#include <ACG/Math/VectorT.hh>
#include <OpenVolumeMesh/Core/Entities.hh>
#include <OpenVolumeMesh/Core/GeometryKernel.hh>

template<class MeshT>
class EntityPosition
{
public:
    EntityPosition(MeshT &mesh) : mesh_(mesh) {}
    ACG::Vec3d operator()(OpenVolumeMesh::VertexHandle vh)
    {
        return mesh_.vertex(vh);
    }
    ACG::Vec3d operator()(OpenVolumeMesh::EdgeHandle eh)
    {
        auto edge = mesh_.edge(eh);
        return 0.5 * (mesh_.vertex(edge.from_vertex()) + mesh_.vertex(edge.to_vertex()));
    }
    ACG::Vec3d operator()(OpenVolumeMesh::HalfEdgeHandle heh)
    {
        auto edge = mesh_.halfedge(heh);
        return (1./3.) * (2 * mesh_.vertex(edge.from_vertex()) + mesh_.vertex(edge.to_vertex()));
    }
    ACG::Vec3d operator()(OpenVolumeMesh::CellHandle ch)
    {
        int count = 0;
        ACG::Vec3d sum{0.,0.,0.};
        for (auto vh: mesh_.cell_vertices(ch)) {
            sum += mesh_.vertex(vh);
            ++count;
        }
        return sum / count;
    }

    ACG::Vec3d operator()(OpenVolumeMesh::FaceHandle fh)
    {
        return (*this)(mesh_.halfface_handle(fh, 0));
    }

    ACG::Vec3d operator()(OpenVolumeMesh::HalfFaceHandle hfh)
    {
        // TODO: maybe offset this slightly from the face barycenter?
        int count = 0;
        ACG::Vec3d sum{0.,0.,0.};
        for (auto vh: mesh_.halfface_vertices(hfh)) {
            sum += mesh_.vertex(vh);
            ++count;
        }
        return sum / count;
    }
private:
    MeshT &mesh_;
};

