#ifndef SCRIPTSETTINGS_HH
#define SCRIPTSETTINGS_HH

#include <QObject>

#if QT_VERSION_MAJOR < 6
#include <QScriptValue>
#endif

class QWidget;

#if QT_VERSION_MAJOR < 6
class QScriptContext;
#endif

class ScriptSettings : public QObject
{
    Q_OBJECT
public:
    explicit ScriptSettings(QWidget *widget);
};

#if QT_VERSION_MAJOR < 6
QScriptValue createSettingsScriptObject(
        QScriptContext *ctx,
        QWidget *widget);
#endif


#endif // SCRIPTSETTINGS_HH
