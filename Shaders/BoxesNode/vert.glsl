#version 140

in vec3 inst_v0;
in vec3 inst_v1;
in vec3 inst_v2;
in vec3 inst_pos;

uniform float scale;

void main()
{
    SG_VERTEX_BEGIN;

    vec4 pa_posOS = SG_INPUT_POSOS;

    vec4 pa_posWS;
    pa_posWS.x = dot(inst_v0, pa_posOS.xyz);
    pa_posWS.y = dot(inst_v1, pa_posOS.xyz);
    pa_posWS.z = dot(inst_v2, pa_posOS.xyz);
    pa_posWS.w = 1.0;
    pa_posWS.xyz *= scale;
    pa_posWS.xyz += inst_pos;


#ifdef SG_INPUT_NORMALOS
    sg_vNormalVS.x = dot(pa_worldTransform0.xyz, SG_INPUT_NORMALOS.xyz);
    sg_vNormalVS.y = dot(pa_worldTransform1.xyz, SG_INPUT_NORMALOS.xyz);
    sg_vNormalVS.z = dot(pa_worldTransform2.xyz, SG_INPUT_NORMALOS.xyz);

    sg_vNormalVS = g_mWVIT * sg_vNormalVS;
    sg_vNormalVS = normalize(sg_vNormalVS);
#endif

    sg_vPosVS = g_mWV * pa_posWS;
    sg_vPosPS = g_mWVP * pa_posWS;

    SG_VERTEX_END;
}
