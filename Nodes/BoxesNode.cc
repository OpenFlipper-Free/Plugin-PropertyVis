/*===========================================================================*\
 *                                                                           *
 *                              OpenFlipper                                  *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
\*===========================================================================*/


#include <ACG/GL/acg_glew.hh>
#include "BoxesNode.hh"
#include <ACG/GL/IRenderer.hh>


namespace ACG {
namespace SceneGraph {

BoxesNode::BoxesNode(BaseNode*    _parent,
                     std::string  _name) :
    MaterialNode(_parent, _name),
    updateVBO_(true)
{
    const std::array<Vec4f,6> dir_color = {
        Vec4f{1.f,0.f,0.f,1.f},
        Vec4f{0.f,1.f,0.f,1.f},
        Vec4f{0.f,0.f,1.f,1.f},
        Vec4f{.4f,0.f,0.f,1.f},
        Vec4f{0.f,.4f,0.f,1.f},
        Vec4f{0.f,0.f,.3f,1.f}
    };

    const std::array<Vec3f, 8> vertices = {
        Vec3f( 1.f, -1.f, -1.f),
        Vec3f( 1.f,  1.f, -1.f),
        Vec3f( 1.f,  1.f,  1.f),
        Vec3f( 1.f, -1.f,  1.f),

        Vec3f(-1.f, -1.f, -1.f),
        Vec3f(-1.f,  1.f, -1.f),
        Vec3f(-1.f,  1.f,  1.f),
        Vec3f(-1.f, -1.f,  1.f)};

    auto add_triangle = [this, &vertices, &dir_color]
            (size_t v0, size_t v1, size_t v2, size_t dim)
    {
        triangles_.push_back({vertices[v0], vertices[v1], vertices[v2]});
        triangle_colors_.push_back(dir_color[dim]);
    };

    triangles_.reserve(6*2);
    triangle_colors_.reserve(6*2);
    add_triangle(0, 1, 2, 0);
    add_triangle(0, 2, 3, 0);
    add_triangle(4, 6, 5, 3);
    add_triangle(4, 7, 6, 3);

    add_triangle(1, 5, 6, 1);
    add_triangle(1, 6, 2, 1);
    add_triangle(0, 3, 7, 4);
    add_triangle(0, 7, 4, 4);

    add_triangle(2, 7, 3, 2);
    add_triangle(2, 6, 7, 2);
    add_triangle(0, 4, 1, 5);
    add_triangle(1, 4, 5, 5);
}

void
BoxesNode::
boundingBox(Vec3d& _bbMin, Vec3d& _bbMax)
{
    // FIXME: this assumes matrices to only consist of vectors up to unit length
    for (const auto &be: elem_) {
        _bbMax.maximize(be.pos + Vec3d(scaleFactor_,scaleFactor_,scaleFactor_));
        _bbMin.minimize(be.pos + Vec3d(-scaleFactor_, -scaleFactor_, -scaleFactor_));
    }
}

void BoxesNode::createVBO()
{
  if (!updateVBO_)
    return;

  // generate if needed:
  geometry_vbo_.bind();
  instance_vbo_.bind();

  vertexDecl_.clear();
  vertexDecl_.addElement(GL_FLOAT, 3, VERTEX_USAGE_POSITION, nullptr, nullptr, 0, geometry_vbo_.id());
  vertexDecl_.addElement(GL_FLOAT, 4, VERTEX_USAGE_COLOR, nullptr, nullptr, 0, geometry_vbo_.id());
  std::vector<float> geom_vbo_data; geom_vbo_data.reserve(3 * (3+4) * triangles_.size());

  vertexDecl_.addElement(GL_FLOAT, 3, VERTEX_USAGE_SHADER_INPUT, nullptr, "inst_v0", 1, instance_vbo_.id());
  vertexDecl_.addElement(GL_FLOAT, 3, VERTEX_USAGE_SHADER_INPUT, nullptr, "inst_v1", 1, instance_vbo_.id());
  vertexDecl_.addElement(GL_FLOAT, 3, VERTEX_USAGE_SHADER_INPUT, nullptr, "inst_v2", 1, instance_vbo_.id());
  vertexDecl_.addElement(GL_FLOAT, 3, VERTEX_USAGE_SHADER_INPUT, nullptr, "inst_pos", 1, instance_vbo_.id());
  std::vector<float> instance_vbo_data; instance_vbo_data.reserve(3 * 4 * elem_.size());

  assert(triangles_.size() == triangle_colors_.size());
  for (size_t i = 0; i < triangles_.size(); ++i)
  {
      auto &tri = triangles_[i];
      auto &color = triangle_colors_[i];
      for (const auto &point: tri) {
          std::copy(point.begin(), point.end(), std::back_inserter(geom_vbo_data));
          std::copy(color.begin(), color.end(), std::back_inserter(geom_vbo_data));
      }
  }
  for (const auto &be: elem_) {
      for (const auto &point: {
           be.transform.getRow(0),
           be.transform.getRow(1),
           be.transform.getRow(2),
           be.pos})
      {
          std::copy(point.begin(), point.end(), std::back_inserter(instance_vbo_data));
      }
  }

  geometry_vbo_.upload(
              static_cast<GLsizeiptr>(geom_vbo_data.size()*sizeof(float)),
              geom_vbo_data.data(),
              GL_STATIC_DRAW_ARB);
  instance_vbo_.upload(
              static_cast<GLsizeiptr>(instance_vbo_data.size()*sizeof(float)),
              instance_vbo_data.data(),
              GL_STATIC_DRAW_ARB);

  // Update done.
  updateVBO_ = false;

}

void
BoxesNode::
getRenderObjects(IRenderer* _renderer, GLState&  _state , const DrawModes::DrawMode& /* _drawMode */, const ACG::SceneGraph::Material* _mat)
{
    if (elem_.empty())
        return;

    RenderObject ro;
    ro.initFromState(&_state);
    ro.setMaterial(_mat);

    boxesNodeName_ = std::string("BoxesNode: ")+name();
    ro.debugName = boxesNodeName_;

    ro.depthTest = true;
    ro.depthWrite = true;

    ro.blending = true;
    ro.blendSrc = GL_SRC_ALPHA;
    ro.blendDest = GL_ONE_MINUS_SRC_ALPHA;

    ro.shaderDesc.shadeMode = SG_SHADE_UNLIT;
    ro.shaderDesc.vertexColors = true;
    ro.shaderDesc.vertexTemplateFile = "BoxesNode/vert.glsl";

    ro.setUniform("scale", scaleFactor_);

    createVBO();
    ro.vertexBuffer = geometry_vbo_.id();
    ro.vertexDecl = &vertexDecl_;

    ro.glDrawInstancedArrays(GL_TRIANGLES, 0,
                             static_cast<GLsizei>(3 * triangles_.size()),
                             static_cast<GLsizei>(elem_.size()));
    _renderer->addRenderObject(&ro);
}

} // namespace SceneGraph
} // namespace ACG
