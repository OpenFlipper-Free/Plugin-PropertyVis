#pragma once
/*===========================================================================*\
 *                                                                           *
 *                              OpenFlipper                                  *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
\*===========================================================================*/


#include <ACG/Scenegraph/MaterialNode.hh>
#include <ACG/Scenegraph/DrawModes.hh>
#include <ACG/GL/VertexDeclaration.hh>
#include <ACG/GL/globjects.hh>
#include <ACG/Math/Matrix3x3T.hh>
#include <vector>
#include <limits>

namespace ACG {
namespace SceneGraph {

struct BoxElement {
    Matrix3x3d transform;
    Vec3d pos;
};

class BoxesNode : public MaterialNode
{
public:
  ACG_CLASSNAME(BoxesNode)

  BoxesNode(BaseNode*    _parent=nullptr,
	          std::string  _name="<BoxesNode>" );

  ~BoxesNode() = default;

  DrawModes::DrawMode availableDrawModes() const override {
      return DrawModes::SOLID_FACES_COLORED_FLAT_SHADED;
  }

  void boundingBox(Vec3d& _bbMin, Vec3d& _bbMax) override;
  
  void clear() { elem_.clear(); updateVBO_ = true; }

  void reserve(size_t _n) { elem_.reserve(_n); }

  /// STL conformance
  void push_back(const BoxElement& _v) { elem_.push_back(_v); updateVBO_ = true; }
  typedef BoxElement         value_type;
  typedef BoxElement&        reference;
  typedef const BoxElement&  const_reference;

  void getRenderObjects(IRenderer* _renderer, GLState&  _state, const DrawModes::DrawMode&  _drawMode, const ACG::SceneGraph::Material* _mat) override;

  void setScaleFactor(float _val) { scaleFactor_ = _val;}

protected:
  /// creates the vbo only if update was requested
  void createVBO();

  std::vector<BoxElement> elem_;
  float scaleFactor_ = 1.0f;

  // geometry of a single box
  std::vector<std::array<Vec3f,3>> triangles_;
  std::vector<Vec4f> triangle_colors_;

  GeometryBuffer geometry_vbo_;
  GeometryBuffer instance_vbo_;

  // True if points changed and the vbo has to be updated
  bool         updateVBO_;

  ACG::VertexDeclaration vertexDecl_;

  std::string boxesNodeName_;
};


//=============================================================================
} // namespace SceneGraph
} // namespace ACG
//=============================================================================
