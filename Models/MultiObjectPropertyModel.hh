/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/


#pragma once

#include "PropertyModel.hh"
#include "PropertyVisualizer/PropertyVisualizer.hh"


class PropertyVisualizer;
class DataType;

class MultiObjectPropertyModel: public PropertyModel
{
    Q_OBJECT

signals:
    void log(Logtype _type, QString _message);
    void log(QString _message);

private slots:
    void slotLog(Logtype _type, QString _message){ emit log(_type, _message); }
    void slotLog(QString _message){ emit log(_message);}

public:
    MultiObjectPropertyModel(const QStringList& res, QObject *parent = 0);
    virtual ~MultiObjectPropertyModel();

    virtual int rowCount(const QModelIndex & parent = QModelIndex()) const override;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    virtual QModelIndex index(int row, int column, QModelIndex const & parent = QModelIndex()) const override;

    /// Revisualizes visualized properties.
    virtual void objectUpdated() override;

    /// Visualizes the selected properties.
    virtual void visualize(QModelIndexList selectedIndices, QWidgetList widgets = QWidgetList()) override;

    /// Removes the selected properties.
    virtual void removeProperty(QModelIndexList selectedIndices) override;

    /// Duplicates the selected properties.
    virtual void duplicateProperty(QModelIndexList selectedIndices) override;

    /// Searches for properties and creates PropertyVisualizers.
    virtual void gatherProperties() override;

    /// Clears the selected property visualization.
    virtual void clear(QModelIndexList selectedIndices) override;

    /// Hides the widget.
    virtual void hideWidget() override;

    /// Returns the widget.
    virtual QWidget* getWidget() override;

    /// Updates the widget
    virtual void updateWidget(const QModelIndexList& selectedIndices) override;

    /// Connects the PropertyVisualizer log signals with the log slot.
    virtual void connectLogs(PropertyVisualizer* propViz) override;

    /// Returns the property info for the property with the given index.
    virtual PropertyInfo getPropertyInfo(const QModelIndex index) const override;

private:
    QWidget* createWidgetForType(const TypeInfoWrapper& info) const;
    void setRange(const PropertyInfo& info, QWidget* widget) const;

private:
    const QStringList restriction;
    const DataType datatypes;
    std::vector<QString> propNames;
    std::vector<PropertyInfo> propInfos;
    std::vector<QWidget*> propWidgets;
    QWidget* widget;
};

